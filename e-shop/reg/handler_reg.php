<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    session_start();
    define('myeshop', true);
    include("../include/db_connect.php");
    include("../functions/functions.php");

    $error = array();
    // inca odata verificam datele introduse in campuri inainte de a fi introduse in baza de date
    $login = strtolower(clear_string($_POST['reg_login']));
    $pass = strtolower(clear_string($_POST['reg_pass']));
    $surname = clear_string($_POST['reg_surname']);

    $name = clear_string($_POST['reg_name']);
    $patronymic = clear_string($_POST['reg_patronymic']);
    $email = clear_string($_POST['reg_email']);

    $phone = clear_string($_POST['reg_phone']);
    $address = clear_string($_POST['reg_address']);


    if (strlen($login) < 5 or strlen($login) > 15) {
        $error[] = "Numele de utilizator trebuie sa contina de la 5 la 15 caractere!";
    } else {
        $result = $connection->query("SELECT login FROM reg_user WHERE login = '$login'");
        If (mysqli_num_rows($result) > 0) {
            $error[] = "Numele de utilizator este ocupat!";
        }

    }

    if (strlen($pass) < 7 or strlen($pass) > 15) $error[] = "Parola trebuie sa contina de la 7 pana la 15 caractere!";
    if (strlen($surname) < 3 or strlen($surname) > 20) $error[] = "Numele trebuie sa contina de la 3 pana la 20 de caractere!";
    if (strlen($name) < 3 or strlen($name) > 15) $error[] = "Prenumele trebuie sa contina de la 3 pana la 15 de caractere!";
    if (strlen($patronymic) < 3 or strlen($patronymic) > 25) $error[] = "Patronimicul trebuie sa contina de la 3 pana la 25 de caractere!";
    if (!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i", trim($email))) $error[] = "Indicati o adresa de  email valida !";
    if (!$phone) $error[] = "Indicati un numar de telefon!";
    if (!$address) $error[] = "Este necesar de a indica adresa de livrare!";


    if (count($error)) {

        echo implode('<br />', $error);

    } else {
        $pass = md5($pass); //codificarea parolei in baza
       $pass = strrev($pass); //inversam parola
       $pass = "9nm2rv8q" . $pass . "2yo6z"; //marim complicitatea parolei

        $ip = $_SERVER['REMOTE_ADDR'];

        $connection->query("	INSERT INTO reg_user(login,pass,surname,name,patronymic,email,phone,address,datetime,ip)
						VALUES(
						
							'" . $login . "',
							'" . $pass . "',
							'" . $surname . "',
							'" . $name . "',
							'" . $patronymic . "',
                            '" . $email . "',
                            '" . $phone . "',
                            '" . $address . "',
                            NOW(),
                            '" . $ip . "'							
						)");

        echo 'true';
    }
}
?>