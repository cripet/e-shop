<?php
define('myeshop', true);
include("include/db_connect.php");
include("functions/functions.php");
session_start();
include("include/auth_cookie.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
    <link href="css/reset.css" rel="stylesheet" type="text/css"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <link href="trackbar/trackbar.css" rel="stylesheet" type="text/css"/>

    <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="js/jcarousellite_1.0.1.js"></script>
    <script type="text/javascript" src="js/shop-script.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="trackbar/jquery.trackbar.js"></script>

    <script type="text/javascript" src="js/jquery.form.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/TextChange.js"></script>
    <link href="css/menu-style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="js/menu-script.js"></script>
    <link rel="icon" href="images/icc.png">

    <script type="text/javascript">
        $(document).ready(function() {
            $('#form_reg').validate(
                {
                     rules:{
                        "reg_login":{
                            required:true,
                            minlength:5,
                            maxlength:15,
                            remote: {
                                type: "post",
                                url: "reg/check_login.php"
                            }
                        },
                        "reg_pass":{
                            required:true,
                            minlength:7,
                            maxlength:15
                        },
                        "reg_surname":{
                            required:true,
                            minlength:3,
                            maxlength:15
                        },
                        "reg_name":{
                            required:true,
                            minlength:3,
                            maxlength:15
                        },
                        "reg_patronymic":{
                            required:true,
                            minlength:3,
                            maxlength:25
                        },
                        "reg_email":{
                            required:true,
                            email:true
                        },
                        "reg_phone":{
                            required:true
                        },
                        "reg_address":{
                            required:true

                        }
                    },

                     messages:{
                        "reg_login":{
                            required:"Introduceti un nume de utilizator!",
                            minlength:"De la 5 pana la 15 caractere!",
                            maxlength:"De la 5 pana la 15 caractere!",
                            remote: "Numele de utilizator este ocupat!"
                        },
                        "reg_pass":{
                            required:"Introduceti o parola!",
                            minlength:"De la 7 pana la 15 caractere!",
                            maxlength:"De la 7 pana la 15 caractere!"
                        },
                        "reg_surname":{
                            required:"Introduceti numele dvoastra!",
                            minlength:"De la 3 pana la 20 caractere!",
                            maxlength:"De la 3 pana la 20 caractere!"
                        },
                        "reg_name":{
                            required:"Introduceti prenumele dvoastra!",
                            minlength:"De la 3 pana la 15 caractere!",
                            maxlength:"De la 3 pana la 15 caractere!"
                        },
                        "reg_patronymic":{
                            required:"Introduceti patronimicul dvoastra!",
                            minlength:"De la 3 pana la 25 caractere!",
                            maxlength:"De la 3 pana la 25 caractere!"
                        },
                        "reg_email":{
                            required:"Adresa de  e-mail este obligatorie",
                            email:"Adresa de e-mail este invalida"
                        },
                        "reg_phone":{
                            required:"Introduceti un numar de telefon!"
                        },
                        "reg_address":{
                            required:"Introduceti adresa pentru livrare!"
                        }
                    },

                    submitHandler: function(form){
                        $(form).ajaxSubmit({
                            success: function(data) {

                                if (data == 'true')
                                {
                                    $("#block-form-registration").fadeOut(300,function() {

                                        $("#reg_message").addClass("reg_message_good").fadeIn(400).html("V-ati inregistrat cu succes!");
                                        $("#form_submit").hide();
                                    });

                                }
                                else
                                {
                                    $("#reg_message").addClass("reg_message_error").fadeIn(400).html(data);
                                }
                            }
                        });
                    }
                });
        });

    </script>
    <title>Inregistrare</title>
</head>
<body>

<div id="block-body">
    <?php
    include("include/block-header.php");
    ?>
    <div id="block-right">
        <?php
        include("include/block-category.php");
        include("include/block-parameter.php");
        ?>
    </div>
    <div id="block-content">

        <h2 class="h2-title">Inregistrare</h2>
        <form method="post" id="form_reg" action="reg/handler_reg.php">
            <p id="reg_message"></p>
            <div id="block-form-registration">
                <ul id="form-registration">

                    <li>
                        <label for="reg_login">Login</label>
                        <span class="star">*</span>
                        <input type="text" name="reg_login" id="reg_login"/>
                    </li>

                    <li>
                        <label for="reg_pass">Parola</label>
                        <span class="star">*</span>
                        <input type="text" name="reg_pass" id="reg_pass"/>
                    </li>

                    <li>
                        <label for="reg_surname">Nume</label>
                        <span class="star">*</span>
                        <input type="text" name="reg_surname" id="reg_surname"/>
                    </li>

                    <li>
                        <label for="reg_name">Prenume</label>
                        <span class="star">*</span>
                        <input type="text" name="reg_name" id="reg_name"/>
                    </li>

                    <li>
                        <label for="reg_patronymic">Patronimic</label>
                        <span class="star">*</span>
                        <input type="text" name="reg_patronymic" id="reg_patronymic"/>
                    </li>

                    <li>
                        <label for="reg_email">E-mail</label>
                        <span class="star">*</span>
                        <input type="text" name="reg_email" id="reg_email"/>
                    </li>

                    <li>
                        <label for="reg_phone">Telefon mobil</label>
                        <span class="star">*</span>
                        <input type="text" name="reg_phone" id="reg_phone"/>
                    </li>

                    <li>
                        <label for="reg_address">Adresa de livrare</label>
                        <span class="star">*</span>
                        <input type="text" name="reg_address" id="reg_address"/>
                    </li>

                    <li>
                       
                    </li>

                </ul>
            </div>

            <p align="right"><input type="submit" name="reg_submit" id="form_submit" value="Trimite"/></p>

        </form>
    </div>

    <?php
    include("include/block-footer.php");
    include "include/block-random.php";
    ?>
</div>
<a href="#top"> <img src="images/Back-to-Top.png" align="right" id="fixedbutton" width="50px"></a>


</body>
</html>