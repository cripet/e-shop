<?php
define('myeshop', true);
include("include/db_connect.php");
include("functions/functions.php");
session_start();
include("include/auth_cookie.php");

$id = clear_string($_GET["id"]);


// numarul de vizualizari se mareste o singura data...
If ($id != $_SESSION['countid']) {
    $querycount = $connection->query("SELECT count FROM table_products WHERE products_id='$id'");
    $resultcount = mysqli_fetch_array($querycount);

    $newcount = $resultcount["count"] + 1;

    $update = $connection->query("UPDATE table_products SET count='$newcount' WHERE products_id='$id'");
}

$_SESSION['countid'] = $id;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
    <link href="css/reset.css" rel="stylesheet" type="text/css"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <link href="trackbar/trackbar.css" rel="stylesheet" type="text/css"/>

    <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="js/jcarousellite_1.0.1.js"></script>
    <script type="text/javascript" src="js/shop-script.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="trackbar/jquery.trackbar.js"></script>
    <script type="text/javascript" src="js/TextChange.js"></script>

    <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css"/>
    <script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript" src="js/jTabs.js"></script>
    <link href="css/menu-style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="js/menu-script.js"></script>
    <link rel="icon" href="images/icc.png">

    <title>Internet Magazin</title>
    <!--  pentru meniul cu descriere si caracteristixi  -->
     <script type="text/javascript">
        $(document).ready(function () {
            $("ul.tabs").jTabs({content: ".tabs_content", animate: true, effect: "slide"});
            $(".image-modal").fancybox();
            $(".send-review").fancybox();

        });
    </script>
</head>
<body>
<div id="block-body">
    <?php
    include("include/block-header.php");
    ?>
    <div id="block-right">
        <?php
        include("include/block-category.php");
        include("include/block-parameter.php");
        ?>
    </div>
    <div id="block-content">
        <?php
        $result1 = $connection->query("SELECT * FROM table_products WHERE products_id='$id' AND visible='1'");
        If (mysqli_num_rows($result1) > 0) {
            $row1 = mysqli_fetch_array($result1);
            do {
                if (strlen($row1["image"]) > 0 && file_exists("uploads_images/" . $row1["image"])) {
                    $img_path = 'uploads_images/' . $row1["image"];
                    $max_width = 300;
                    $max_height = 300;
                    list($width, $height) = getimagesize($img_path);
                    $ratioh = $max_height / $height;
                    $ratiow = $max_width / $width;
                    $ratio = min($ratioh, $ratiow);

                    if($row1['type_prod']=='notebook' || $row1['type_prod']=='')
                    {
                        $width=180;
                        $height=120;
                    }
                    else
                    {
                        $width = intval($ratio * $width);
                        $height = intval($ratio * $height);
                    }

                } else {
                    $img_path = "images/no-image.png";
                    $width = 110;
                    $height = 200;
                }

                $query_reviews = $connection->query("SELECT * FROM table_reviews WHERE products_id = '$id' AND moderat='1'");
                $count_reviews = mysqli_num_rows($query_reviews);


                echo '
                    <div id="block-content-info">
                    
                    <img src="' . $img_path . '" width="' . $width . '" height="' . $height . '" />
                    
                    <div id="block-mini-description">
                    
                    <p id="content-title">' . $row1["title"] . '</p>
                    
                    <ul class="reviews-and-counts-content">
                    <li><img src="images/eye-icon.png" /><p>' . $row1["count"] . '</p></li>
                    <li><img src="images/comment-icon.png" /><p>' . $count_reviews . '</p></li>
                    </ul>
                    
                    <p id="style-price" >' . group_numerals($row1["price"]) . ' lei</p>
                    <a class="add-cart" id="add-cart-view" tid="' . $row1["products_id"] . '" ></a>
                    <p id="content-text">' . $row1["mini_description"] . '</p>
                    </div>
                    </div>
';

            } while ($row1 = mysqli_fetch_array($result1));


            $result = $connection->query("SELECT * FROM table_products WHERE products_id='$id' AND visible='1'");
            $row = mysqli_fetch_array($result);

            echo '
                <ul class="tabs">
                    <li><a class="active" href="#" >Descriere</a></li>
                    <li><a href="#" >Caracteristici</a></li>
                </ul>
                
                <div class="tabs_content">
                <div>' . $row["description"] . '</div>
                <div>' . $row["features"] . '</div>
                <div>

';

        }
        ?>
    </div>

    <?php
    include("include/block-random.php");
    include("include/block-footer.php");
     ?>
</div>
<a href="#top"> <img src="images/Back-to-Top.png" align="right" id="fixedbutton" width="50px"></a>

</body>
</html>