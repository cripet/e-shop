<?php
define('myeshop', true);
include("include/db_connect.php");
include("functions/functions.php");
session_start();
include("include/auth_cookie.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
    <link href="css/reset.css" rel="stylesheet" type="text/css"/>
    <link href="css/menu-style.css" rel="stylesheet" type="text/css"/>

    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <link href="trackbar/trackbar.css" rel="stylesheet" type="text/css"/>

    <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="js/jcarousellite_1.0.1.js"></script>
    <script type="text/javascript" src="js/shop-script.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="trackbar/jquery.trackbar.js"></script>
    <script type="text/javascript" src="js/TextChange.js"></script>
    <script type="text/javascript" src="js/menu-script.js"></script>
    <link rel="icon" href="images/icc.png">


    <title>Internet magazin </title>
</head>
<body>
<div id="block-body">
    <?php
    include("include/block-header.php");
    ?>

        <div id="block-contacts">
            <div id="block-form">
                <form action="MAILTO:ppcristea@gmail.com" method="POST" enctype="multipart/form-data">
                    <div class="row"><br/>
                        <label for="name">Numele tau:</label><br/>
                        <input name="name" type="text" value="" size="30"/><br/>
                    </div>
                    <div class="row"><br/>
                        <label for="email">Adresa de email:</label><br/>
                        <input name="email" type="text" value="" size="30"/><br/>
                    </div>
                    <div class="row"><br/>
                        <label for="phone">Numarul de mobil:</label><br/>
                        <input name="phone" size="30" type="text"/ ><br/>
                    </div>
                    <div class="row"><br/>
                        <label for="message">Lasa-ne un mesaj:</label><br/>
                        <textarea id="message" name="message" rows="7" cols="32" typeof="text"></textarea><br/>
                    </div>

                    <input id="button-search" type="submit" value="Trimite"/>
                </form>
            </div>


            <div id="block-map">
                <p>Ne gasiti aici...</p>
<!--                <img src="images/map.png" width="600" height="500"/>-->
                            <iframe width="600" height="500"
                                    src="http://www.maps.ie/create-google-map/map.php?width=600&amp;height=500&amp;hl=en&amp;q=str.%20alexe%20mateevici%2C%20chisinau+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=A&amp;output=embed"
                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0" >
                            </iframe>
            </div>
            <br/>
        </div>
    <?php
    include("include/block-random.php");
    include("include/block-footer.php");
    ?>  </div>
   <a href="#top"> <img src="images/Back-to-Top.png" align="right" id="fixedbutton" width="50px"></a>


</body>
</html>