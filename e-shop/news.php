<?php
define('myeshop', true);
include("include/db_connect.php");
include("functions/functions.php");
session_start();
include("include/auth_cookie.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href="css/reset.css" rel="stylesheet" type="text/css"/>
    <link href="css/menu-style.css" rel="stylesheet" type="text/css"/>

    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <link href="trackbar/trackbar.css" rel="stylesheet" type="text/css"/>

    <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="js/jcarousellite_1.0.1.js"></script>
    <script type="text/javascript" src="js/shop-script.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="trackbar/jquery.trackbar.js"></script>
    <script type="text/javascript" src="js/TextChange.js"></script>
    <script type="text/javascript" src="js/menu-script.js"></script>
    <link rel="icon" href="images/icc.png">


    <title>Internet magazin </title>
</head>
<body>
<div id="block-body">
    <?php
    include("include/block-header.php");
    ?>
    <div id="block-right">
        <?php
        include("include/block-category.php");
        include("include/block-parameter.php");
        ?>
    </div>
    <div id="block-content">

            <?php $num = 10;
            $page = (int)$_GET['page'];

            $count = $connection->query("SELECT COUNT(*) FROM news");
            $temp = mysqli_fetch_array($count);

            If ($temp[0] > 0) {
                $tempcount = $temp[0];

                $total = (($tempcount - 1) / $num) + 1;
                $total = intval($total);

                $page = intval($page);

                if (empty($page) or $page < 0) $page = 1;

                if ($page > $total) $page = $total;

                $start = $page * $num - $num;

                $qury_start_num = " LIMIT $start, $num";
            }


            $result = $connection->query("SELECT * FROM news   $qury_start_num");

            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_array($result);



                do {
                    echo '
<div id="block-new">
  <p class="new-title" ><b>' . $row["title"] . '</b></p>  
  <h5 class="new-date">'.$row['date'].'</h5>
  <p class="new-text">' . $row["text"] . '</strong> </p>

</div>
  ';

                } while ($row = mysqli_fetch_array($result));
            }
            ?>

         <?php


        if ($page != 1) {
            $pstr_prev = '<li><a class="pstr-prev" href="news.php?page=' . ($page - 1) . '">&lt;</a></li>';
        }
        if ($page != $total) $pstr_next = '<li><a class="pstr-next" href="news.php?page=' . ($page + 1) . '">&gt;</a></li>';


        if ($page - 5 > 0) $page5left = '<li><a href="news.php?page=' . ($page - 5) . '">' . ($page - 5) . '</a></li>';
        if ($page - 4 > 0) $page4left = '<li><a href="news.php?page=' . ($page - 4) . '">' . ($page - 4) . '</a></li>';
        if ($page - 3 > 0) $page3left = '<li><a href="news.php?page=' . ($page - 3) . '">' . ($page - 3) . '</a></li>';
        if ($page - 2 > 0) $page2left = '<li><a href="news.php?page=' . ($page - 2) . '">' . ($page - 2) . '</a></li>';
        if ($page - 1 > 0) $page1left = '<li><a href="news.php?page=' . ($page - 1) . '">' . ($page - 1) . '</a></li>';

        if ($page + 5 <= $total) $page5right = '<li><a href="news.php?page=' . ($page + 5) . '">' . ($page + 5) . '</a></li>';
        if ($page + 4 <= $total) $page4right = '<li><a href="news.php?page=' . ($page + 4) . '">' . ($page + 4) . '</a></li>';
        if ($page + 3 <= $total) $page3right = '<li><a href="news.php?page=' . ($page + 3) . '">' . ($page + 3) . '</a></li>';
        if ($page + 2 <= $total) $page2right = '<li><a href="news.php?page=' . ($page + 2) . '">' . ($page + 2) . '</a></li>';
        if ($page + 1 <= $total) $page1right = '<li><a href="news.php?page=' . ($page + 1) . '">' . ($page + 1) . '</a></li>';


        if ($page + 5 < $total) {
            $strtotal = '<li><p class="nav-point">...</p></li><li><a href="news.php?page=' . $total . '">' . $total . '</a></li>';
        } else {
            $strtotal = "";
        }

        if ($total > 1) {
            echo '
    <div class="pstrnav">
    <ul>
    ';
            echo $pstr_prev . $page5left . $page4left . $page3left . $page2left . $page1left . "<li><a class='pstr-active' href='news.php?page=" . $page . "'>" . $page . "</a></li>" . $page1right . $page2right . $page3right . $page4right . $page5right . $strtotal . $pstr_next;
            echo '
    </ul>
    </div>
    ';
        }
        ?>

</div>
    <?php
    include("include/block-random.php");
    include("include/block-footer.php");
    ?>
</div>
<a href="#top"> <img src="images/Back-to-Top.png" align="right" id="fixedbutton" width="50px"></a>

</body>
</html>