<?php
defined('myeshop') or die('Acces interzis!');
$db_host		= 'localhost';
$db_user		= 'root';
$db_pass		= '';
$db_database	= 'db_shop';

$connection = new mysqli($db_host,$db_user,$db_pass, $db_database);

if ($connection->connect_error) {
    die("Eroare la conectare: " . $connection->connect_error);
}

?>