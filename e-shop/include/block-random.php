<?php
defined('myeshop') or die('Acces interzis!');
?>

<div id="block-random-news">
    <div id="block-news">
        <div id="newsticker">
            <ul>

                <?php
                $result = $connection->query("SELECT * FROM news");
                If (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_array($result);
                    do {
                        echo '
<li>
<span>' . $row["date"] . '</span>
<a href="#">' . $row["title"] . '</a>
<p id="random-new-text" >' . $row["text"] . '</p>
</li>

';

                    } while ($row = mysqli_fetch_array($result));
                }
                ?>
            </ul>
        </div>

    </div>
</div>