<?php
defined('myeshop') or die('Acces interzis!');
?>
<div id="block-category">
    <p class="header-title">Categorii de produse</p>

    <ul>

        <li><a id="index1"><img src="images/mobile-icon.gif" id="mobile-images"/>Telefoane mobile</a>
            <ul class="category-section">
                <li><a href="view_cat.php?type=mobile"><strong>Toate modelele</strong> </a></li>
                <?php
                $result = $connection->query("SELECT * FROM category WHERE type='mobile'");
                If (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_array($result);
                    do {
                        echo '
                        <li><a href="view_cat.php?cat=' . strtolower($row["brand"]) . '&type=' . $row["type"] . '">' . $row["brand"] . '</a></li>';
                    } while ($row = mysqli_fetch_array($result));
                }
                ?>
            </ul>
        </li>

        <li><a id="index2"><img src="images/book-icon.gif" id="book-images"/>Notebook-uri</a>
            <ul class="category-section">
                <li><a href="view_cat.php?type=notebook"><strong>Toate modelele</strong> </a></li>

                <?php

                $result = $connection->query("SELECT * FROM category WHERE type='notebook'");

                If (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_array($result);
                    do {
                        echo '
    
  <li><a href="view_cat.php?cat=' . strtolower($row["brand"]) . '&type=' . $row["type"] . '">' . $row["brand"] . '</a></li>
    
    ';
                    } while ($row = mysqli_fetch_array($result));
                }

                ?>

            </ul>
        </li>

        <li><a id="index3"><img src="images/table-icon.gif" id="table-images"/>Tablete</a>
            <ul class="category-section">
                <li><a href="view_cat.php?type=notepad"><strong>Toate modelele</strong> </a></li>
                <?php

                $result = $connection->query("SELECT * FROM category WHERE type='notepad'");

                If (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_array($result);
                    do {
                        echo '
        
  <li><a href="view_cat.php?cat=' . strtolower($row["brand"]) . '&type=' . $row["type"] . '">' . $row["brand"] . '</a></li>
    
    ';
                    } while ($row = mysqli_fetch_array($result));
                }

                ?>
            </ul>
        </li>

    </ul>

</div>