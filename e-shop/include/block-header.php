<?php

defined('myeshop') or die('Acces interzis!');
?>
<div id="block-header">
    <div id="header-top-block">
        <ul id="header-top-menu">
            <li id="block-basket"><img src="images/cart-icon.png"/><a href="cart.php?action=oneclick"> Cos gol</a>
                </li>
        </ul>

        <?php

        if ($_SESSION['auth'] == 'yes_auth') { 

            echo '<p id="auth-user-info" align="right"><img src="./images/user.png" />Salut, ' . $_SESSION['auth_name'] . '!</p>';

        } else {

            echo '<p id="reg-auth-title" align="right"><a class="top-auth">Intra</a><a href="./registration.php">Inregistrare</a></p>';

        }

        ?>
        
        <div id="block-top-auth">
            <div class="corner"></div>
            <form method="post">
                <ul id="input-email-pass">
                    <p id="message-auth">Login sau (si) parola gresita</p>
                    <li>
                        <center><input type="text" id="auth_login" placeholder="login sau e-mail"/></center>
                    </li>
                    <li>
                        <center><input type="password" id="auth_pass" placeholder="parola"/></center>
                    </li>

                    <ul id="list-auth">
                        <li><input type="checkbox" name="rememberme" id="rememberme"/><label for="rememberme">Tine-ma
                                logat</label></li>
                    </ul>


                    <p align="right" id="button-auth"><a>Intra</a></p>

                    <p align="right" class="auth-loading"><img src="images/loading.gif"/></p>

                </ul>
            </form>


            <div id="block-remind">
                <h3>Recuperarea<br/> parolei </h3>
                <p id="message-remind" class="message-remind-success"></p>
                <center><input type="text" id="remind-email" placeholder="adresa de e-mail"/></center>
                <p align="right" id="button-remind"><a>OK</a></p>
                <p align="right" class="auth-loading"><img src="images/loading.gif"/></p>
            </div>


        </div>

    </div>

    <div id="top-line"></div>

    <div id="block-user">
        <div class="corner2"></div>
        <ul >
            <li><img src="images/user_info.png"/><a href="./profile.php">Profil</a></li>
            <li><img src="images/logout.png"/><a id="logout">Iesire</a></li>
        </ul>
    </div>


    <img id="img-logo" src="images/logo2.png"/>

    <div id="personal-info">
        <p align="right">Apel gratuit.</p>
        <h3 align="right">(224) 56 456</h3>
        <img src="images/phone.png" width="34"/>
        <p align="right">Orar de lucru:</p>
        <p align="right">Zile de lucru: 8:00 - 18:00</p>
        <p align="right">Sambata, Duminica - zi libere</p>
        <img src="images/orar.png" width="34"/>
    </div>

    <div id="block-search">
        <form method="GET" action="search.php?q=">
            <span></span>
            <input type="text" id="input-search" name="q" placeholder="Cauta printre produse..."
                   value="<?php echo $search; ?>"/>
            <input type="submit" id="button-search" value="Cauta"/>
        </form>
        <ul id="result-search">


        </ul>

    </div>
</div>
<div id="cssmenu">
    <ul>
        <li><a href="index.php">Acasa</a></li>
        <li><a href="new_products.php">Articole noi</a></li>
        <li><a href="bestsellers.php">Cele mai vandute</a></li>
        <li><a href="news.php">Noutati</a></li>
        <li><a href="#">Despre noi</a></li>
        <li><a href="#">Magazine</a></li>
        <li><a href="contacts.php">Contacte</a></li>

    </ul>
    <div id="nav-line"></div>
</div>