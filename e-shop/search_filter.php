<?php
define('myeshop', true);
include("include/db_connect.php");
include("functions/functions.php");
session_start();
include("include/auth_cookie.php");

$cat = clear_string($_GET['cat']);
$type = clear_string($_GET['type']);



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
    <link href="css/reset.css" rel="stylesheet" type="text/css"/>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <link href="trackbar/trackbar.css" rel="stylesheet" type="text/css"/>

    <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="js/jcarousellite_1.0.1.js"></script>
    <script type="text/javascript" src="js/shop-script.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="trackbar/jquery.trackbar.js"></script>
    <script type="text/javascript" src="js/TextChange.js"></script>
    <link href="css/menu-style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="js/menu-script.js"></script>
    <link rel="icon" href="images/icc.png">

    <title>Cautare dupa parametri </title>
</head>
<body>
<div id="block-body">
    <?php
    include("include/block-header.php");
    ?>
    <div id="block-right">
        <?php
        include("include/block-category.php");
        include("include/block-parameter.php");
        ?>
    </div>
    <div id="block-content">
        <ul id="block-prod">

            <?php
            if($_GET['brand'])
            {
                $check_brand = implode(',',(array)$_GET['brand']);
            }

            $start_price = (int)$_GET['start_price'];
            $end_price = (int)$_GET['end_price'];

            if(!empty($check_brand) || !empty($end_price))
            {
                if(!empty($check_brand)) $query_brand = " AND brand_id IN($check_brand)";
                if(!empty($end_price)) $query_price = " AND price BETWEEN $start_price AND $end_price";
            }

            $num = 6;
            $page = (int)$_GET['page'];

            $count = $connection->query("SELECT COUNT(*) FROM table_products WHERE visible = '1' $query_brand $query_price ");
            $temp = mysqli_fetch_array($count);

            If ($temp[0] > 0) {
                $tempcount = $temp[0];

                $total = (($tempcount - 1) / $num) + 1;
                $total = intval($total);

                $page = intval($page);

                if (empty($page) or $page < 0) $page = 1;

                if ($page > $total) $page = $total;

                $start = $page * $num - $num;

                $qury_start_num = " LIMIT $start, $num";
            }


            $result = $connection->query("SELECT * FROM table_products WHERE visible='1' $query_brand $query_price $qury_start_num ");

            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_array($result); //

                do {

                    if ($row["image"] != "" && file_exists("uploads_images/" . $row["image"])) {
                        $img_path = 'uploads_images/' . $row["image"];
                        $max_width = 200;
                        $max_height = 200;
                        list($width, $height) = getimagesize($img_path);
                        $ratioh = $max_height / $height;
                        $ratiow = $max_width / $width;
                        $ratio = min($ratioh, $ratiow);
                        if($row['type_prod']=='notebook' || $row['type_prod']=='notepad')
                        {
                            $width=150;
                            $height=100;
                        }
                        else
                        {
                            $width = intval($ratio * $width);
                            $height = intval($ratio * $height);
                        }

                    } else {
                        $img_path = "images/no-image.png";
                        $width = 110;
                        $height = 200;
                    }

                    $query_reviews = $connection->query("SELECT * FROM table_reviews WHERE products_id = '{$row["products_id"]}' AND moderat='1'");
                    $count_reviews = mysqli_num_rows($query_reviews);


                    echo '
  <li>
  <div class="block-images-grid" >
  <img src="' . $img_path . '" width="' . $width . '" height="' . $height . '" />
  </div>
  <p class="style-title-grid" ><a href="http://shop//mobile/' . $row["products_id"] . '-' . $row["title"] . '/" >' . $row["title"] . '</a></p>
  <ul class="reviews-and-counts-grid">
    <li><img src="images/eye-icon.png" /><p>' . $row["count"] . '</p></li>
    <li><img src="images/comment-icon.png" /><p>' . $count_reviews . '</p></li>
  </ul>
  <a class="add-cart-style-grid" tid="' . $row["products_id"] . '" ></a>
  <p class="style-price-grid" ><strong>' . group_numerals($row["price"]) . '</strong> lei.</p>
  <div class="mini-features" >
  ' . $row["mini_features"] . '
  </div>
  </li>
 
  ';

                } while ($row = mysqli_fetch_array($result));
            }
            ?>

        </ul>


                  <?php
        

        if ($page != 1) {
            $pstr_prev = '<li><a class="pstr-prev" href="search_filter.php?page=' . ($page - 1) . '">&lt;</a></li>';
        }
        if ($page != $total) $pstr_next = '<li><a class="pstr-next" href="search_filter.php?page=' . ($page + 1) . '">&gt;</a></li>';

        if ($page - 5 > 0) $page5left = '<li><a href="search_filter.php?page=' . ($page - 5) . '">' . ($page - 5) . '</a></li>';
        if ($page - 4 > 0) $page4left = '<li><a href="search_filter.php?page=' . ($page - 4) . '">' . ($page - 4) . '</a></li>';
        if ($page - 3 > 0) $page3left = '<li><a href="search_filter.php?page=' . ($page - 3) . '">' . ($page - 3) . '</a></li>';
        if ($page - 2 > 0) $page2left = '<li><a href="search_filter.php?page=' . ($page - 2) . '">' . ($page - 2) . '</a></li>';
        if ($page - 1 > 0) $page1left = '<li><a href="search_filter.php?page=' . ($page - 1) . '">' . ($page - 1) . '</a></li>';

        if ($page + 5 <= $total) $page5right = '<li><a href="search_filter.php?page=' . ($page + 5) . '">' . ($page + 5) . '</a></li>';
        if ($page + 4 <= $total) $page4right = '<li><a href="search_filter.php?page=' . ($page + 4) . '">' . ($page + 4) . '</a></li>';
        if ($page + 3 <= $total) $page3right = '<li><a href="search_filter.php?page=' . ($page + 3) . '">' . ($page + 3) . '</a></li>';
        if ($page + 2 <= $total) $page2right = '<li><a href="search_filter.php?page=' . ($page + 2) . '">' . ($page + 2) . '</a></li>';
        if ($page + 1 <= $total) $page1right = '<li><a href="search_filter.php?page=' . ($page + 1) . '">' . ($page + 1) . '</a></li>';


        if ($page + 5 < $total) {
            $strtotal = '<li><p class="nav-point">...</p></li><li><a href="search_filter.php?page=' . $total . '">' . $total . '</a></li>';
        } else {
            $strtotal = "";
        }

        if ($total > 1) {
            echo '
    <div class="pstrnav">
    <ul>
    ';
            echo $pstr_prev . $page5left . $page4left . $page3left . $page2left . $page1left . "<li><a class='pstr-active' href='search_filter.php?page=" . $page . "'>" . $page . "</a></li>" . $page1right . $page2right . $page3right . $page4right . $page5right . $strtotal . $pstr_next;
            echo '
    </ul>
    </div>
    ';
        }


        ?>


    </div>

    <?php
    include("include/block-random.php");
    include("include/block-footer.php");
    ?>
</div>
<a href="#top"> <img src="images/Back-to-Top.png" align="right" id="fixedbutton" width="50px"></a>

</body>
</html>