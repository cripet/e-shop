<?php
session_start();
if ($_SESSION['auth_admin'] == "yes_auth") {
    define('myeshop', true);

    if (isset($_GET["logout"])) {
        unset($_SESSION['auth_admin']);
        header("Location: login.php");
    }


    include("include/db_connect.php");
    include("include/functions.php");

    if ($_POST["submit_add"]) {
        if ($_SESSION['add_prod'] == '1') {

            $error = array();


            if (!$_POST["form_title"]) {
                $error[] = "Indicati denumirea produsului";
            }

            if (!$_POST["form_price"]) {
                $error[] = "Indicati pretul";
            }

            if (!$_POST["form_category"]) {
                $error[] = "Indicati categoria";
            } else {
                $result = $connection->query("SELECT * FROM category WHERE id='{$_POST["form_category"]}'");
                $row = mysqli_fetch_array($result);
                $selectbrand = $row["brand"];

            }


            if ($_POST["chk_visible"]) {
                $chk_visible = "1";
            } else {
                $chk_visible = "0";
            }

            if ($_POST["chk_new"]) {
                $chk_new = "1";
            } else {
                $chk_new = "0";
            }

            if ($_POST["chk_leader"]) {
                $chk_leader = "1";
            } else {
                $chk_leader = "0";
            }

            if ($_POST["chk_sale"]) {
                $chk_sale = "1";
            } else {
                $chk_sale = "0";
            }


            if (count($error)) {
                $_SESSION['message'] = "<p id='form-error'>" . implode('<br />', $error) . "</p>";

            } else {

                $connection->query("INSERT INTO table_products(title,price,brand,mini_description,description,mini_features,features,datetime,new,leader,sale,visible,type_prod,brand_id)
						VALUES(						
                            '" . $_POST["form_title"] . "',
                            '" . $_POST["form_price"] . "',
                            '" . $selectbrand . "',
                            '" . $_POST["txt1"] . "',
                            '" . $_POST["txt2"] . "',
                            '" . $_POST["txt3"] . "',
                            '" . $_POST["txt4"] . "',
                            NOW(),
                            '" . $chk_new . "',
                            '" . $chk_leader . "',
                            '" . $chk_sale . "',
                            '" . $chk_visible . "',
                            '" . $_POST["form_type"] . "',
                            '" . $_POST["form_category"] . "'                               
						)");

                $_SESSION['message'] = "<p id='form-success'>Produsul a fost adaugat cu success!</p>";
                $id = mysqli_insert_id($connection);

                if (empty($_POST["upload_image"])) {
                    include("actions/upload-image.php");
                    unset($_POST["upload_image"]);
                }

            }


        } else {
            $msgerror = 'Nu aveti dreptul de a adauga produse!';
        }
    }

    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="jquery_confirm/jquery_confirm.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script type="text/javascript" src="./ckeditor/ckeditor.js"></script>
        <title>Adaugare produs</title>
    </head>
    <body>
    <div id="block-body">
        <?php
        include("include/block-header.php");
        ?>
        <div id="block-content">
            <div id="block-parameters">
                <p id="title-page">Adaugare produs</p>
            </div>
            <?php
            if (isset($msgerror)) echo '<p id="form-error" align="center">' . $msgerror . '</p>';

            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }

            if (isset($_SESSION['answer'])) {
                echo $_SESSION['answer'];
                unset($_SESSION['answer']);
            }
            ?>

            <form enctype="multipart/form-data" method="post">
                <ul id="edit-prod">

                    <li>
                        <label>Denumirea produsului</label>
                        <input type="text" name="form_title"/>
                    </li>

                    <li>
                        <label>Pretul</label>
                        <input type="text" name="form_price"/>
                    </li>

                    <li>
                        <label>Tipul produsului</label>
                        <select name="form_type" id="type" size="1">

                            <option value="mobile">Telefoane mobil</option>
                            <option value="notebook">Notebook-uri</option>
                            <option value="notepad">Tablete</option>

                        </select>
                    </li>

                    <li>
                        <label>Categoria</label>
                        <select name="form_category" size="10">

                            <?php
                            $category = $connection->query("SELECT * FROM category");

                            If (mysqli_num_rows($category) > 0) {
                                $result_category = mysqli_fetch_array($category);
                                do {

                                    echo '
  
  <option value="' . $result_category["id"] . '" >' . $result_category["brand"] . '</option>
  
  ';

                                } while ($result_category = mysqli_fetch_array($category));
                            }
                            ?>

                        </select>
                </ul>
                <label class="stylelabel">Imaginea de baza</label>

                <div id="baseimg-upload">
                    <input type="hidden" name="MAX_FILE_SIZE" value="5000000"/>
                    <input type="file" name="upload_image"/>

                </div>

                <h3 class="h3click">Descriere scurta</h3>
                <div class="div-editor1">
                    <textarea id="editor1" name="txt1" cols="100" rows="20"></textarea>
                    <script type="text/javascript">
                        var ckeditor1 = CKEDITOR.replace("editor1");
                        AjexFileManager.init({
                            returnTo: "ckeditor",
                            editor: ckeditor1
                        });
                    </script>
                </div>

                <h3 class="h3click">Descrierea produsului</h3>
                <div class="div-editor2">
                    <textarea id="editor2" name="txt2" cols="100" rows="20"></textarea>
                    <script type="text/javascript">
                        var ckeditor1 = CKEDITOR.replace("editor2");
                        AjexFileManager.init({
                            returnTo: "ckeditor",
                            editor: ckeditor1
                        });
                    </script>
                </div>

                <h3 class="h3click">Descrierea scurta a caracteristicilor</h3>
                <div class="div-editor3">
                    <textarea id="editor3" name="txt3" cols="100" rows="20"></textarea>
                    <script type="text/javascript">
                        var ckeditor1 = CKEDITOR.replace("editor3");
                        AjexFileManager.init({
                            returnTo: "ckeditor",
                            editor: ckeditor1
                        });
                    </script>
                </div>

                <h3 class="h3click">Caracteristici</h3>
                <div class="div-editor4">
                    <textarea id="editor4" name="txt4" cols="100" rows="20"></textarea>
                    <script type="text/javascript">
                        var ckeditor1 = CKEDITOR.replace("editor4");
                        AjexFileManager.init({
                            returnTo: "ckeditor",
                            editor: ckeditor1
                        });
                    </script>
                </div>

                <h3 class="h3title">Setarile produsului</h3>
                <ul id="chkbox">
                    <li><input type="checkbox" name="chk_visible" id="chk_visible"/><label for="chk_visible">Fa produsul
                            vizibil</label></li>
                    <li><input type="checkbox" name="chk_new" id="chk_new"/><label for="chk_new">Produs nou</label></li>
                    <li><input type="checkbox" name="chk_leader" id="chk_leader"/><label for="chk_leader">Produs
                            popular</label></li>
                    <li><input type="checkbox" name="chk_sale" id="chk_sale"/><label for="chk_sale">Reduceri</label>
                    </li>
                </ul>


                <p align="right"><input type="submit" id="submit_form" name="submit_add" value="Adauga produsul"/></p>
            </form>


        </div>
    </div>
    </body>
    </html>
    <?php
} else {
    header("Location: login.php");
}
?>