<?php
defined('myeshop') or die('Acces interzis!');
$error_img = array();

if ($_FILES['upload_image']['error'] > 0) {
    switch ($_FILES['upload_image']['error']) {
        case 1:
            $error_img[] = 'Marimea fisierului intrece marimea permisa UPLOAD_MAX_FILE_SIZE';
            break;
        case 2:
            $error_img[] = 'Marimea fisierului intrece marimea permisa AX_FILE_SIZE';
            break;
        case 3:
            $error_img[] = 'Nu s-a reusit de incarcat o parte din fisier';
            break;
        case 4:
            $error_img[] = 'Fisierul n-a fost incarcat';
            break;
        case 6:
            $error_img[] = 'Lispeste folderul temporar.';
            break;
        case 7:
            $error_img[] = 'Nu s-a putut de scris fisierul pe disc.';
            break;
        case 8:
            $error_img[] = 'Extensa PHP a oprit incarcarea fiserului.';
            break;
    }

} else {
    //verificam extensia
    if ($_FILES['upload_image']['type'] == 'image/jpeg' || $_FILES['upload_image']['type'] == 'image/jpg' || $_FILES['upload_image']['type'] == 'image/png') {

        $uploaddir = '../uploads_images/';
        $name = $_FILES['upload_image']['name'];
        $uploadfile = $uploaddir . $name;

        if (move_uploaded_file($_FILES['upload_image']['tmp_name'], $uploadfile)) {

            $update = $connection->query("UPDATE table_products SET image='$name' WHERE products_id = '$id'");

        } else {
            $error_img[] = "Eroare la incarcarea fisierului.";
        }


    } else {
        $error_img[] = 'Extensii permise: jpeg, jpg, png';
    }
}


?>