$(document).ready(function() {

	$('.delete').click(function(){
		
		var rel = $(this).attr("rel");
		
		$.confirm({
			'title'		: 'Confirmarea stergerii',
			'message'	: 'Dupa stergere recuperarea  va fi imposibila. Continuati?',
			'buttons'	: {
				'Da'	: {
					'class'	: 'blue',
					'action': function(){
						location.href = rel;
					}
				},
				'Nu'	: {
					'class'	: 'gray',
					'action': function(){}
				}
			}
		});
		
	});
    
    

 $('.h3click').click(function(){ 
 $(this).next().slideToggle(400); 
 });


//   var count_input = 1;
//
//   $("#add-input").click(function(){
//
//   count_input++;
//
//   $('<div id="addimage'+count_input+'" class="addimage"><input type="hidden" name="MAX_FILE_SIZE" value="2000000"/><input type="file" name="galleryimg[]" /><a class="delete-input" rel="'+count_input+'" >Sterge</a></div>').fadeIn(300).appendTo('#objects');
//
//   });
//
//   $('.delete-input').live('click',function(){
//
// 	var rel = $(this).attr("rel");
//
// 	$("#addimage"+rel).fadeOut(300,function(){
//     $("#addimage"+rel).remove();
// 	});
//
// });



$('.delete-cat').click(function(){
    
   var selectid = $("#cat_type option:selected").val();
     
   if (!selectid)
   {
       $("#cat_type").css("borderColor","#F5A4A4");
   }else
   {
  $.ajax({
  type: "POST",
  url: "./actions/delete-category.php",
  data: "id="+selectid,
  dataType: "html",
  cache: false,
  success: function(data) {
    
  if (data == "delete")
  {
     $("#cat_type option:selected").remove();
  }
                          }
         });       
   }
              
});
 
   $('.block-clients').click(function(){

 $(this).find('ul').slideToggle(300);
   
 });
 
 
 $('#select-all').click(function(){
    $(".privilege input:checkbox").attr('checked', true);           
});

$('#remove-all').click(function(){
    $(".privilege input:checkbox").attr('checked', false);           
});
 
 
   
 });