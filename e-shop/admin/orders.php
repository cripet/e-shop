<?php
session_start();
if ($_SESSION['auth_admin'] == "yes_auth")
{
	define('myeshop', true);
       
       if (isset($_GET["logout"]))
    {
        unset($_SESSION['auth_admin']);
        header("Location: login.php");
    }


  include("include/db_connect.php");
  include("include/functions.php"); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="jquery_confirm/jquery_confirm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script> 
    <script type="text/javascript" src="js/script.js"></script> 
    <script type="text/javascript" src="jquery_confirm/jquery_confirm.js"></script> 
    
	<title>Panou de control</title>
</head>
<body>
<div id="block-body">
<?php
	include("include/block-header.php");
    
 $all_count = $connection->query("SELECT * FROM orders");
 $all_count_result = mysqli_num_rows($all_count);

 $buy_count = $connection->query("SELECT * FROM orders WHERE order_confirmed = 'yes'");
 $buy_count_result = mysqli_num_rows($buy_count);

 $no_buy_count = $connection->query("SELECT * FROM orders WHERE order_confirmed = 'no'");
 $no_buy_count_result = mysqli_num_rows($no_buy_count);
 
?>
<div id="block-content">
<div id="block-parameters">
<ul id="options-list">
<li>Comenzi</li>

</ul>
</div>
<div id="block-info">
</div>
<?php
	$result = $connection->query("SELECT * FROM orders");
 
 If (mysqli_num_rows($result) > 0)
{
$row = mysqli_fetch_array($result);
do
{
if ($row["order_confirmed"] == 'yes')
{
    $status = '<span class="green">Prelucrat</span>';
} else
{
    $status = '<span class="red">Neprelucrat</span>';
}
  
 echo '
 <div class="block-order">
 
  <p class="order-datetime" >'.$row["order_datetime"].'</p>
  <p class="order-number" >Comanda nr. '.$row["order_id"].' - '.$status.'</p>
  <p class="order-link" ><a class="green" href="view_order.php?id='.$row["order_id"].'" >Detalii</a></p>
 </div>
 ';   
    
} while ($row = mysqli_fetch_array($result));
}
?>
</div>
</div>
</body>
</html>
<?php
}else
{
    header("Location: login.php");
}
?>