<?php
session_start();
if ($_SESSION['auth_admin'] == "yes_auth") {
    define('myeshop', true);

    if (isset($_GET["logout"])) {
        unset($_SESSION['auth_admin']);
        header("Location: login.php");
    }
    
    include("include/db_connect.php");
    include("include/functions.php");
    $id = clear_string($_GET["id"]);
    $action = clear_string($_GET["action"]);
    if (isset($action)) {
        switch ($action) {

            case 'delete':

                if ($_SESSION['edit_prod'] == '1') {

                    if (file_exists("../uploads_images/" . $_GET["img"])) {
                        unlink("../uploads_images/" . $_GET["img"]);
                    }

                } else {
                    $msgerror = 'A aparut o eroare la editarea produsului!';
                }

                break;

        }
    }
    if ($_POST["submit_save"]) {
        if ($_SESSION['edit_prod'] == '1') {
            $error = array();


            if (!$_POST["form_title"]) {
                $error[] = "Indicati denumirea produsului";
            }

            if (!$_POST["form_price"]) {
                $error[] = "Indicati pretul";
            }

            if (!$_POST["form_category"]) {
                $error[] = "Indicati categoria";
            } else {
                $result = $connection->query("SELECT * FROM category WHERE id='{$_POST["form_category"]}'");
                $row = mysqli_fetch_array($result);
                $selectbrand = $row["brand"];

            }


            if (empty($_POST["upload_image"])) {
                include("actions/upload-image.php");
                unset($_POST["upload_image"]);
            }
 


            if ($_POST["chk_visible"]) {
                $chk_visible = "1";
            } else {
                $chk_visible = "0";
            }

            if ($_POST["chk_new"]) {
                $chk_new = "1";
            } else {
                $chk_new = "0";
            }

            if ($_POST["chk_leader"]) {
                $chk_leader = "1";
            } else {
                $chk_leader = "0";
            }

            if ($_POST["chk_sale"]) {
                $chk_sale = "1";
            } else {
                $chk_sale = "0";
            }


            if (count($error)) {
                $_SESSION['message'] = "<p id='form-error'>" . implode('<br />', $error) . "</p>";

            } else {

                $querynew = "title='{$_POST["form_title"]}',price='{$_POST["form_price"]}',brand='$selectbrand',mini_description='{$_POST["txt1"]}',description='{$_POST["txt2"]}',mini_features='{$_POST["txt3"]}',features='{$_POST["txt4"]}',new='$chk_new',leader='$chk_leader',sale='$chk_sale',visible='$chk_visible',type_prod='{$_POST["form_type"]}',brand_id='{$_POST["form_category"]}'";

                $update = $connection->query("UPDATE table_products SET $querynew WHERE products_id = '$id'");

                $_SESSION['message'] = "<p id='form-success'>Produsul a fost editat cu succes!</p>";

            }
        } else {
            $msgerror = 'Nu aveti dreptul de a edita produse!';
        }
    }

    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="jquery_confirm/jquery_confirm.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script type="text/javascript" src="./ckeditor/ckeditor.js"></script>
        <title>Editare produs</title>
    </head>
    <body>
    <div id="block-body">
        <?php
        include("include/block-header.php");
        ?>
        <div id="block-content">
            <div id="block-parameters">
                <p id="title-page">Editare produs</p>
            </div>
            <?php
            if (isset($msgerror)) echo '<p id="form-error" align="center">' . $msgerror . '</p>';

            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }

            if (isset($_SESSION['answer'])) {
                echo $_SESSION['answer'];
                unset($_SESSION['answer']);
            }
            ?>
            <?php
            $result = $connection->query("SELECT * FROM table_products WHERE products_id='$id'");

            If (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_array($result);
                do {

                    echo '

<form enctype="multipart/form-data" method="post">
<ul id="edit-prod">

<li>
<label>Denumirea produsului</label>
<input type="text" name="form_title" value="' . $row["title"] . '" />
</li>

<li>
<label>Pretul</label>
<input type="text" name="form_price" value="' . $row["price"] . '"  />
</li>

';

                    $category = $connection->query("SELECT * FROM category");

                    If (mysqli_num_rows($category) > 0) {
                        $result_category = mysqli_fetch_array($category);

                        if ($row["type_prod"] == "mobile") $type_mobile = "selected";
                        if ($row["type_prod"] == "notebook") $type_notebook = "selected";
                        if ($row["type_prod"] == "notepad") $type_notepad = "selected";


                        echo '
<li>
<label>Tipul produsului</label>
<select name="form_type" id="type" size="1" >

<option ' . $type_mobile . ' value="mobile" >Telefoane mobile</option>
<option ' . $type_notebook . ' value="notebook" >Notebook-uri</option>
<option ' . $type_notepad . ' value="notepad" >Tablete</option>

</select>
</li>

<li>
<label>Categoria</label>
<select name="form_category" size="10" >
';


                        do {

                            echo '
  
  <option value="' . $result_category["id"] . '" >' . $result_category["type"] . '-' . $result_category["brand"] . '</option>
  
  ';

                        } while ($result_category = mysqli_fetch_array($category));
                    }
                    echo '
</select>
</ul> 
';

                    if (strlen($row["image"]) > 0 && file_exists("../uploads_images/" . $row["image"])) {
                        $img_path = '../uploads_images/' . $row["image"];
                        $max_width = 110;
                        $max_height = 110;
                        list($width, $height) = getimagesize($img_path);
                        $ratioh = $max_height / $height;
                        $ratiow = $max_width / $width;
                        $ratio = min($ratioh, $ratiow);
// New dimensions 
                        $width = intval($ratio * $width);
                        $height = intval($ratio * $height);

                        echo '
<label class="stylelabel" >Imaginea de baza</label>
<div id="baseimg">
<img src="' . $img_path . '" width="' . $width . '" height="' . $height . '" />
<a href="edit_product.php?id=' . $row["products_id"] . '&img=' . $row["image"] . '&action=delete" ></a>
</div>

';

                    } else {
                        echo '
<label class="stylelabel" >Imaginea de baza</label>

<div id="baseimg-upload">
<input type="hidden" name="MAX_FILE_SIZE" value="5000000"/>
<input type="file" name="upload_image" />

</div>
';
                    }

                    echo '
<h3 class="h3click" >Descriere scurta</h3>
<div class="div-editor1" >
<textarea id="editor1" name="txt1" cols="100" rows="20">' . $row["mini_description"] . '</textarea>
		<script type="text/javascript">
			var ckeditor1 = CKEDITOR.replace( "editor1" );
			AjexFileManager.init({
				returnTo: "ckeditor",
				editor: ckeditor1
			});
		</script>
 </div>       
 
<h3 class="h3click" >Descrierea produsului</h3>
<div class="div-editor2" >
<textarea id="editor2" name="txt2" cols="100" rows="20">' . $row["description"] . '</textarea>
		<script type="text/javascript">
			var ckeditor1 = CKEDITOR.replace( "editor2" );
			AjexFileManager.init({
				returnTo: "ckeditor",
				editor: ckeditor1
			});
		</script>
 </div>          

<h3 class="h3click" >Descrierea scurta a caracteristicilor</h3>
<div class="div-editor3" >
<textarea id="editor3" name="txt3" cols="100" rows="20">' . $row["mini_features"] . '</textarea>
		<script type="text/javascript">
			var ckeditor1 = CKEDITOR.replace( "editor3" );
			AjexFileManager.init({
				returnTo: "ckeditor",
				editor: ckeditor1
			});
		</script>
 </div>        

<h3 class="h3click" >Caracteristici</h3>
<div class="div-editor4" >
<textarea id="editor4" name="txt4" cols="100" rows="20">' . $row["features"] . '</textarea>
		<script type="text/javascript">
			var ckeditor1 = CKEDITOR.replace( "editor4" );
			AjexFileManager.init({
				returnTo: "ckeditor",
				editor: ckeditor1
			});
		</script>
  </div> 
';


                    if ($row["visible"] == '1') $checked1 = "checked";
                    if ($row["new"] == '1') $checked2 = "checked";
                    if ($row["leader"] == '1') $checked3 = "checked";
                    if ($row["sale"] == '1') $checked4 = "checked";


                    echo ' 
<h3 class="h3title" >Setarile produsului</h3>   
<ul id="chkbox">
<li><input type="checkbox" name="chk_visible" id="chk_visible" ' . $checked1 . ' /><label for="chk_visible" >Fa produsul vizibil</label></li>
<li><input type="checkbox" name="chk_new" id="chk_new" ' . $checked2 . ' /><label for="chk_new" >Produs nou</label></li>
<li><input type="checkbox" name="chk_leader" id="chk_leader" ' . $checked3 . ' /><label for="chk_leader" >Produs popular</label></li>
<li><input type="checkbox" name="chk_sale" id="chk_sale" ' . $checked4 . ' /><label for="chk_sale" >Reduceri</label></li>
</ul> 


    <p align="right" ><input type="submit" id="submit_form" name="submit_save" value="Salveaza modificarile"/></p>     
</form>
';

                } while ($row = mysqli_fetch_array($result));
            }
            ?>


        </div>
    </div>
    </body>
    </html>
    <?php
} else {
    header("Location: login.php");
}
?>