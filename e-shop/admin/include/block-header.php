<?php
defined('myeshop') or die('Acces interzis!');

$result1 = $connection->query("SELECT * FROM orders WHERE order_confirmed='no'");
$count1 = mysqli_num_rows($result1);

if ($count1 > 0) {
    $count_str1 = '<p>+' . $count1 . '</p>';
} else {
    $count_str1 = '';
}

$result2 = $connection->query("SELECT * FROM table_reviews WHERE moderat='0'");
$count2 = mysqli_num_rows($result2);

if ($count2 > 0) {
    $count_str2 = '<p>+' . $count2 . '</p>';
} else {
    $count_str2 = '';
}

?>
<div id="block-header">

    <div id="block-header1">
        <h3>E-SHOP. Panoul de control</h3>
        <p id="link-nav"><a href="index.php">Acasa</a></p>
    </div>

    <div id="block-header2">
        <p align="right"><a href="administrators.php">Administratori</a> | <a href="?logout">Iesire</a></p>
        <p align="right">Dvs - <span><?php echo $_SESSION['admin_role']; ?></span></p>
    </div>

</div>

<div id="left-nav">
    <ul>
        <li><a href="orders.php">Comenzi</a><?php echo $count_str1; ?></li>
        <li><a href="product.php">Produse</a></li>
        <li><a href="#">Recenzii</a></li>
        <li><a href="category.php">Categorii</a></li>
        <li><a href="clients.php">Clienti</a></li>
        <li><a href="news.php">Stiri</a></li>
    </ul>
</div>