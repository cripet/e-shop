<?php
session_start();
if ($_SESSION['auth_admin'] == "yes_auth") {
    define('myeshop', true);

    if (isset($_GET["logout"])) {
        unset($_SESSION['auth_admin']);
        header("Location: login.php");
    }


    include("include/db_connect.php");
    include("include/functions.php");
    $id = clear_string($_GET["id"]);

    if ($_POST["submit_edit"]) {
        if ($_SESSION['auth_admin_login'] == 'admin') {

            $error = array();

            if (!$_POST["admin_login"]) $error[] = "Indicati loginul!";
            if ($_POST["admin_pass"]) {
                $pass = md5(clear_string($_POST["admin_pass"]));
                $pass = strrev($pass);
                $pass = "pass='" . strtolower("mb03foo51" . $pass . "qj2jjdp9") . "',";
            }

            if (!$_POST["admin_fullname"]) $error[] = "Indicati numele complet!";
            if (!$_POST["admin_role"]) $error[] = "Indicati rolul!";
            if (!$_POST["admin_email"]) $error[] = "Indicati E-mail!";

            if (count($error)) {
                $_SESSION['message'] = "<p id='form-error'>" . implode('<br />', $error) . "</p>";
            } else {


                $querynew = "login='{$_POST["admin_login"]}',$pass fullname='{$_POST["admin_fullname"]}',role='{$_POST["admin_role"]}',email='{$_POST["admin_email"]}',phone='{$_POST["admin_phone"]}',view_orders='{$_POST["view_orders"]}',accept_orders='{$_POST["accept_orders"]}',delete_orders='{$_POST["delete_orders"]}',add_prod='{$_POST["add_prod"]}',edit_prod='{$_POST["edit_prod"]}',delete_prod='{$_POST["delete_prod"]}',accept_reviews='{$_POST["accept_reviews"]}',delete_reviews='{$_POST["delete_reviews"]}',view_clients='{$_POST["view_clients"]}',delete_clients='{$_POST["delete_clients"]}',add_news='{$_POST["add_news"]}',delete_news='{$_POST["delete_news"]}',add_category='{$_POST["add_category"]}',delete_category='{$_POST["delete_category"]}',view_admin='{$_POST["view_admin"]}'";

                $update = $connection->query("UPDATE reg_admin SET $querynew WHERE id = '$id'");

                $_SESSION['message'] = "<p id='form-success'>Datele au fost editate cu succes !</p>";
            }

        } else {
            $msgerror = 'Acces interzis!';
        }
    }

    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="jquery_confirm/jquery_confirm.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script type="text/javascript" src="jquery_confirm/jquery_confirm.js"></script>
        <title>Panou de control - Editare admin.</title>
    </head>
    <body>
    <div id="block-body">
        <?php
        include("include/block-header.php");
        ?>
        <div id="block-content">
            <div id="block-parameters">
                <p id="title-page">Editarea datelor personale</p>
            </div>
            <?php
            if (isset($msgerror)) echo '<p id="form-error" align="center">' . $msgerror . '</p>';

            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }

            $result = $connection->query("SELECT * FROM reg_admin WHERE id='$id'");

            If (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_array($result);
                do {
                    if ($row["view_orders"] == "1") $view_orders = "checked";
                    if ($row["accept_orders"] == "1") $accept_orders = "checked";
                    if ($row["delete_orders"] == "1") $delete_orders = "checked";
                    if ($row["add_prod"] == "1") $add_prod = "checked";
                    if ($row["edit_prod"] == "1") $edit_prod = "checked";
                    if ($row["delete_prod"] == "1") $delete_prod = "checked";
                    if ($row["accept_reviews"] == "1") $accept_reviews = "checked";
                    if ($row["delete_reviews"] == "1") $delete_reviews = "checked";
                    if ($row["view_clients"] == "1") $view_clients = "checked";
                    if ($row["delete_clients"] == "1") $delete_clients = "checked";
                    if ($row["add_news"] == "1") $add_news = "checked";
                    if ($row["delete_news"] == "1") $delete_news = "checked";
                    if ($row["view_admin"] == "1") $view_admin = "checked";
                    if ($row["add_category"] == "1") $add_category = "checked";
                    if ($row["delete_category"] == "1") $delete_category = "checked";


                    echo '
                        <form method="post" id="form-info" >
                        
                        <ul id="info-admin">
                        <li><label>Login</label><input type="text" name="admin_login" value="' . $row["login"] . '" /></li>
                        <li><label>Parola</label><input type="password" name="admin_pass"  /></li>
                        <li><label>Nume complet</label><input type="text" name="admin_fullname" value="' . $row["fullname"] . '" /></li>
                        <li><label>Rol</label><input type="text" name="admin_role" value="' . $row["role"] . '" /></li>
                        <li><label>E-mail</label><input type="text" name="admin_email" value="' . $row["email"] . '" /></li>
                        <li><label>Telefon</label><input type="text" name="admin_phone" value="' . $row["phone"] . '" /></li>
                        </ul>
                        
                        <h3 id="title-privilege" >Privilegii</h3>
                        
                        <p id="link-privilege"><a id="select-all" >Selecteaza toate</a> | <a id="remove-all" >Deselecteaza toate</a></p>
                        
                        <div class="block-privilege">
                        
                        <ul class="privilege">
                        <li><h3>Comenzi</h3></li>
                        
                        <li>
                        <input type="checkbox" name="view_orders" id="view_orders" value="1" ' . $view_orders . ' />
                        <label for="view_orders">Vizualizarea comenzilor.</label>
                        </li>
                        
                        <li>
                        <input type="checkbox" name="accept_orders" id="accept_orders" value="1" ' . $accept_orders . ' />
                        <label for="accept_orders">Prelucrarea comenzilor.</label>
                        </li>
                        
                        <li>
                        <input type="checkbox" name="delete_orders" id="delete_orders" value="1" ' . $delete_orders . ' />
                        <label for="delete_orders">Stergerea comenzilor.</label>
                        </li>
                        
                        </ul>
                        <ul class="privilege">
                        <li><h3>Produse</h3></li>
                        
                        <li>
                        <input type="checkbox" name="add_prod" id="add_prod" value="1" ' . $add_prod . ' />
                        <label for="add_prod">Adaugare produs.</label>
                        </li>
                        
                        <li>
                        <input type="checkbox" name="edit_prod" id="edit_prod" value="1" ' . $edit_prod . ' />
                        <label for="edit_prod">Editare produs.</label>
                        </li>
                        
                        <li>
                        <input type="checkbox" name="delete_prod" id="delete_prod" value="1" ' . $delete_prod . ' />
                        <label for="delete_prod">Stergere produs.</label>
                        </li>
                        
                        </ul>
                        
                        <ul class="privilege">
                        <li><h3>Recenzii</h3></li>
                        
                        <li>
                        <input type="checkbox" name="accept_reviews" id="accept_reviews" value="1" ' . $accept_reviews . ' />
                        <label for="accept_reviews">Moderarea recenziilor.</label>
                        </li>
                        
                        <li>
                        <input type="checkbox" name="delete_reviews" id="delete_reviews" value="1" ' . $delete_reviews . ' />
                        <label for="delete_reviews">Stergerea recenziilor.</label>
                        </li>
                        
                        </ul>
                        
                        </div>
                        <div class="block-privilege">
                        
                        <ul class="privilege">
                        <li><h3>Clienti</h3></li>
                        
                        <li>
                        <input type="checkbox" name="view_clients" id="view_clients" value="1" ' . $view_clients . ' />
                        <label for="view_clients">Vizualizarea clientilor.</label>
                        </li>
                        
                        <li>
                        <input type="checkbox" name="delete_clients" id="delete_clients" value="1" ' . $delete_clients . ' />
                        <label for="delete_clients">Stergerea clientilor.</label>
                        </li>
                        
                        </ul>
                        
                        <ul class="privilege">
                        <li><h3>Stiri</h3></li>
                        
                        <li>
                        <input type="checkbox" name="add_news" id="add_news" value="1" ' . $add_news . ' />
                        <label for="add_news">Adaugare stire.</label>
                        </li>
                        
                        
                        <li>
                        <input type="checkbox" name="delete_news" id="delete_news" value="1" ' . $delete_news . ' />
                        <label for="delete_news">Stergere stire.</label>
                        </li>
                        
                        </ul>
                        
                        <ul class="privilege">
                        <li><h3>Categorii</h3></li>
                        
                        <li>
                        <input type="checkbox" name="add_category" id="add_category" value="1" ' . $add_category . ' />
                        <label for="add_category">Adaugare categorie.</label>
                        </li>
                        
                        <li>
                        <input type="checkbox" name="delete_category" id="delete_category" value="1" ' . $delete_category . ' />
                        <label for="delete_category">Stergere categorie.</label>
                        </li>
                        
                        </ul>
                        
                        </div>
                        
                        <div class="block-privilege">
                        
                        <ul class="privilege">
                        <li><h3>Administratori</h3></li>
                        
                        <li>
                        <input type="checkbox" name="view_admin" id="view_admin" value="1" ' . $view_admin . ' />
                        <label for="view_admin">Vizualizare administratori.</label>
                        </li>
                        
                        </ul>
                        
                        </div>
                        
                        <p align="right"><input type="submit" id="submit_form" name="submit_edit" value="Salveaza"/></p>
                        </form>

';

                } while ($row = mysqli_fetch_array($result));
            }

            ?>

        </div>
    </div>
    </body>
    </html>
    <?php
} else {
    header("Location: login.php");
}
?>