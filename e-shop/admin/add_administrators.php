<?php
session_start();
if ($_SESSION['auth_admin'] == "yes_auth") {
    define('myeshop', true);

    if (isset($_GET["logout"])) {
        unset($_SESSION['auth_admin']);
        header("Location: login.php");
    }


    include("include/db_connect.php");
    include("include/functions.php");

    if ($_POST["submit_add"]) {
        if ($_SESSION['auth_admin_login'] == 'admin') {

            $error = array();

            if ($_POST["admin_login"]) {
                $login = clear_string($_POST["admin_login"]);
                $query = $connection->query("SELECT login FROM reg_admin WHERE login='$login'");

                If (mysqli_num_rows($query) > 0) {
                    $error[] = "Loginul este ocupat!";
                }


            } else {
                $error[] = "Indicati loginul!";
            }


            if (!$_POST["admin_pass"]) $error[] = "Introduceti o parola!";
            if (!$_POST["admin_fullname"]) $error[] = "Indicati numele complet!";
            if (!$_POST["admin_role"]) $error[] = "Indicati rolul!";
            if (!$_POST["admin_email"]) $error[] = "Indicati E-mail!";

            if (count($error)) {
                $_SESSION['message'] = "<p id='form-error'>" . implode('<br />', $error) . "</p>";
            } else {
                $pass = md5(clear_string($_POST["admin_pass"]));
                $pass = strrev($pass);
                $pass = strtolower("mb03foo51" . $pass . "qj2jjdp9");

                $connection->query("INSERT INTO reg_admin(login,pass,fullname,role,email,phone,view_orders,accept_orders,delete_orders,add_prod,edit_prod,delete_prod,accept_reviews,delete_reviews,view_clients,delete_clients,add_news,delete_news,add_category,delete_category,view_admin)
						VALUES(						
                            '" . clear_string($_POST["admin_login"]) . "',
                            '" . $pass . "',
                            '" . clear_string($_POST["admin_fullname"]) . "',
                            '" . clear_string($_POST["admin_role"]) . "',
                            '" . clear_string($_POST["admin_email"]) . "',
                            '" . clear_string($_POST["admin_phone"]) . "',
                            '" . $_POST["view_orders"] . "',
                            '" . $_POST["accept_orders"] . "',
                            '" . $_POST["delete_orders"] . "',							
                            '" . $_POST["add_prod"] . "',
                            '" . $_POST["edit_prod"] . "',                            
							'" . $_POST["delete_prod"] . "',
                            '" . $_POST["accept_reviews"] . "',
                            '" . $_POST["delete_reviews"] . "',
							'" . $_POST["view_clients"] . "',
                            '" . $_POST["delete_clients"] . "',
							'" . $_POST["add_news"] . "',							
							'" . $_POST["delete_news"] . "',
							'" . $_POST["add_category"] . "',
							'" . $_POST["delete_category"] . "',
                            '" . $_POST["view_admin"] . "'                                                                                                                                            
						)");

                $_SESSION['message'] = "<p id='form-success'>Un nou administrator a fost adaugat!</p>";
            }

        } else {
            $msgerror = 'Nu aveti dreptul de a adauga administratori!';
        }
    }
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="jquery_confirm/jquery_confirm.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script type="text/javascript" src="jquery_confirm/jquery_confirm.js"></script>
        <title>Panou de control - Administratori</title>
    </head>
    <body>
    <div id="block-body">
        <?php
        include("include/block-header.php");
        ?>
        <div id="block-content">
            <div id="block-parameters">
                <p id="title-page">Adaugare administrator</p>
            </div>
            <?php
            if (isset($msgerror)) echo '<p id="form-error" align="center">' . $msgerror . '</p>';

            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            ?>
            <form method="post" id="form-info">

                <ul id="info-admin">
                    <li><label>Login</label><input type="text" name="admin_login"/></li>
                    <li><label>Parola</label><input type="password" name="admin_pass"/></li>
                    <li><label>Nume complet</label><input type="text" name="admin_fullname"/></li>
                    <li><label>Rol</label><input type="text" name="admin_role"/></li>
                    <li><label>E-mail</label><input type="text" name="admin_email"/></li>
                    <li><label>Telefon</label><input type="text" name="admin_phone"/></li>
                </ul>

                <h3 id="title-privilege">Privilegii</h3>

                <p id="link-privilege"><a id="select-all">Selecteaza toate</a> | <a id="remove-all">Deselecteaza toate</a></p>

                <div class="block-privilege">

                    <ul class="privilege">
                        <li><h3>Comenzi</h3></li>

                        <li>
                            <input type="checkbox" name="view_orders" id="view_orders" value="1"/>
                            <label for="view_orders">Vizualizarea comenzilor.</label>
                        </li>

                        <li>
                            <input type="checkbox" name="accept_orders" id="accept_orders" value="1"/>
                            <label for="accept_orders">Prelucrarea comenzilor.</label>
                        </li>

                        <li>
                            <input type="checkbox" name="delete_orders" id="delete_orders" value="1"/>
                            <label for="delete_orders">Stergerea comenzilor.</label>
                        </li>

                    </ul>
                    <ul class="privilege">
                        <li><h3>Produse</h3></li>

                        <li>
                            <input type="checkbox" name="add_prod" id="add_prod" value="1"/>
                            <label for="add_prod">Adaugare produs.</label>
                        </li>

                        <li>
                            <input type="checkbox" name="edit_prod" id="edit_prod" value="1"/>
                            <label for="edit_prod">Editare produs.</label>
                        </li>

                        <li>
                            <input type="checkbox" name="delete_prod" id="delete_prod" value="1"/>
                            <label for="delete_prod">Stergere produs.</label>
                        </li>

                    </ul>

                    <ul class="privilege">
                        <li><h3>Recenzii</h3></li>

                        <li>
                            <input type="checkbox" name="accept_reviews" id="accept_reviews" value="1"/>
                            <label for="accept_reviews">Moderarea recenziilor.</label>
                        </li>

                        <li>
                            <input type="checkbox" name="delete_reviews" id="delete_reviews" value="1"/>
                            <label for="delete_reviews">Stergerea recenziilor.</label>
                        </li>

                    </ul>

                </div>
                <div class="block-privilege">

                    <ul class="privilege">
                        <li><h3>Clienti</h3></li>

                        <li>
                            <input type="checkbox" name="view_clients" id="view_clients" value="1"/>
                            <label for="view_clients">Vizualizarea clientilor.</label>
                        </li>

                        <li>
                            <input type="checkbox" name="delete_clients" id="delete_clients" value="1"/>
                            <label for="delete_clients">Stergerea clientilor.</label>
                        </li>

                    </ul>

                    <ul class="privilege">
                        <li><h3>Stiri</h3></li>

                        <li>
                            <input type="checkbox" name="add_news" id="add_news" value="1"/>
                            <label for="add_news">Adaugare stire.</label>
                        </li>


                        <li>
                            <input type="checkbox" name="delete_news" id="delete_news" value="1"/>
                            <label for="delete_news">Stergere stire.</label>
                        </li>

                    </ul>

                    <ul class="privilege">
                        <li><h3>Categorii</h3></li>

                        <li>
                            <input type="checkbox" name="add_category" id="add_category" value="1"/>
                            <label for="add_category">Adaugare categorie.</label>
                        </li>

                        <li>
                            <input type="checkbox" name="delete_category" id="delete_category" value="1"/>
                            <label for="delete_category">Stergere categorie.</label>
                        </li>

                    </ul>

                </div>

                <div class="block-privilege">

                    <ul class="privilege">
                        <li><h3>Administratori</h3></li>

                        <li>
                            <input type="checkbox" name="view_admin" id="view_admin" value="1"/>
                            <label for="view_admin">Vizualizare administratori.</label>
                        </li>

                    </ul>

                </div>

                <p align="right"><input type="submit" id="submit_form" name="submit_add" value="Adauga"/></p>
            </form>
        </div>
    </div>
    </body>
    </html>
    <?php
} else {
    header("Location: login.php");
}
?>